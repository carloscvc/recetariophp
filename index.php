<?php

session_start(); 

if (empty($_SESSION['user'])) {		
 	$_SESSION['user']='invitado';	
}

$userSession = $_SESSION['user'];

include 'lib/config.php';

$template = $twig->loadTemplate("index.html");

include_once 'funciones.php';

$datos = dameListaRecetas($userSession);

echo $template->render($datos);

?>