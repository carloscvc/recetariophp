<?php

function dameListaRecetas($userSession){
	
	try {
		
		$conn = new PDO('sqlite:recetas.sqlite3');
	
		$listado = '
					SELECT *, re.id AS id, usu.id AS id_usuario, usu.username
				     	FROM  recetas_receta re
				      	JOIN  auth_user usu
				      	ON (re.creador_id = usu.id)
						ORDER BY fecha_modificacion DESC
			      		;
					';
					
		$resultado = $conn->query($listado);
	
		foreach($resultado as $re){
			$registros[] = $re;
		}
		
		if (empty($registros)) {
			$registros="";
		}
	
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	$conn = null;

	$datos = array(
			'listarecetas' => $registros,
			'userSession' => $userSession
	);
	return $datos;
}
function dameResultadoBusqueda($userSession, $nomReceta, $tipoReceta, $username){
	
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetas.sqlite3');
	
		$consulta = $conn->prepare('
									SELECT *, usu.id AS id_usuario, usu.username
								     	FROM  recetas_receta re
								      	JOIN  auth_user usu ON (re.creador_id = usu.id)
										WHERE re.nombre LIKE :nomRe
											AND re.tipo LIKE :tipoRe
											AND usu.username LIKE :usu
										ORDER BY fecha_modificacion DESC;
									'
								);
		
		
		$consulta->bindParam(":nomRe", $nomRe);
		$consulta->bindParam(":tipoRe", $tipRe);
		$consulta->bindParam(":usu", $usu);
		
		$nomRe = "%".$nomReceta."%";
		$tipRe = "%".$tipoReceta."%";
		$usu = "%".$username."%";
		
		$consulta->execute();
		
		$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
	
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	$conn = null;
	
	//renderizar plantilla
	$datos = array(
			'listarecetas' => $registros,
			'userSession' => $userSession
	);
	return $datos;
	
}

function añadeReceta($idusu, $nomReceta, $fechaCre, $fechaMod, $ingredientes, $tipo, $elaboracion, $tiempo){
	
	try {
		$conn = new PDO('sqlite:recetas.sqlite3');
		$consulta = $conn->prepare('
									INSERT INTO recetas_receta (nombre, fecha_creacion, fecha_modificacion, ingredientes, tipo, elaboracion, tiempo, imagen ) 
										VALUES (
												:nomrec,
												:fechac,
												:fecham,
												:ing,
												:tip,
												:elab,
												:tiem,
												:img
												);
								   '
								);
								
		$consulta->bindParam(":nomrec", $nomrec);
		$consulta->bindParam(":fechac", $fechac);
		$consulta->bindParam(":fecham", $fecham);
		$consulta->bindParam(":ing", $ing);
		$consulta->bindParam(":tip", $tip);
		$consulta->bindParam(":elab", $elab);
		$consulta->bindParam(":tiem", $tiem);
		$consulta->bindParam(":img", $img);
				
		$nomrec = $nomReceta;
		$fechac = $fechaCre;
		$fecham = $fechaMod;
		$ing = $ingredientes;
		$tip = $tipoPlato;
		$elab = $elaboracion;
		$tiem = $tiempo;
		$img = $imagen;
		
		$consulta->execute();
	
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}

	$conn = null;
}

function dameRecetaPorId($userSession, $idrec){
	
	try {
		$conn = new PDO('sqlite:recetas.sqlite3');
		$consulta = $conn->prepare('
									SELECT *, re.id AS id_receta, usu.id AS id_usuario, usu.username
										FROM  recetas_receta re
										JOIN  auth_user usu
										ON (re.creador_id = usu.id)
										WHERE re.id=:idrec
										;
									'
									);
		$consulta->bindParam(":idrec", $id_re);
		$id_re = $idrec;
		$consulta->execute();
		$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	catch(PDOException $e){
		echo $e->getMessage();
	} 

	$conn = null;
}

function editaReceta($idre, $idusu, $imagen, $nomReceta, $tipoPlato, $ingredientes, $elaboracion, $fechaCre, $fechaMod, $tiempo){
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetas.sqlite3');
		$consulta = $conn->prepare('
									UPDATE recetas_receta
										SET 
											nombre =:nomrec, 
											tipo =:tip, 
											ingredientes =:ing, 
											elaboracion =:elab,
											fecha_modificacion =:fecham, 
											tiempo =:tiem, 
											imagen =:img
										WHERE recetas_receta.id=:idre; 
								   '
								);
		$consulta->bindParam(":nomrec", $nomrec);
		$consulta->bindParam(":tip", $tip);
		$consulta->bindParam(":ing", $ing);
		$consulta->bindParam(":elab", $elab);
		$consulta->bindParam(":fecham", $fecham);
		$consulta->bindParam(":tiem", $tiem);
		$consulta->bindParam(":img", $img);
		$consulta->bindParam(":idre", $id_re);
		
		$nomrec = $nomReceta;
		$tip = $tipoPlato;
		$ing = $ingredientes;
		$elab = $elaboracion;
		$fecham = $fechaMod;
		$tiem = $tiempo;
		$img = $imagen;
		$id_re = $idre;
		
		$consulta->execute();
	
	}
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	$conn = null;
}

function eliminarReceta($idrec){
	
	try {

		$conn = new PDO('sqlite:recetario.sqlite');
		
		$consulta = $conn->prepare('
									DELETE FROM recetas_receta WHERE recetas_receta.id=:idrec;
								   '
								  );
								  
		$consulta->bindParam(":idrec", $id_re);
		
		$id_re = $idrec;
		
		$consulta->execute();
		
		$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	$conn = null;
	
	$datos = array(
			'camposreceta' => $registros,
			'userSession' => $userSession
	);
	
	return $datos;
}

function comprobarUserPass($usu, $pass){
	
	$pass = 'carlos';
		
	$valido = false;
	
	if (empty($usu) || empty($pass)) {	
		$valido = false;
	}else {

		try {
			
			$conn = new PDO('sqlite:recetas.sqlite3');
			$consulta = $conn->prepare('
										SELECT username, password 
											FROM auth_user 
											WHERE username=:us AND password=:psw
											;									
										'
									  );
		
			$consulta->bindParam(":us", $us);
			$consulta->bindParam(":psw", $psw);
		
			$us = $usu;
			$psw = $pass;
			
			$consulta->execute();

			$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
			
			if (!empty($registros)) {
				$valido = true;
			}else {
				$valido = false;
			}
		
		}
		catch(PDOException $e){
		echo $e->getMessage();
		} 
		
		$conn = null;
		
		$datos = array(
			'camposreceta' => $registros,
			'userSession' => $userSession
		);
	}
	return $valido;
}

function muestraAviso($aviso, $link){
	
	include 'lib/config.php';
	$template = $twig->loadTemplate("aviso.html");
	
	$datos = array(
					'aviso' => $aviso,
					'link' => $link
					);
	echo $template->render($datos);
			
}

?>







