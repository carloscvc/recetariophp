<?php

	session_start();
	
	$userSession = $_SESSION['user'];
	
	include 'lib/config.php';
	
	$template = $twig->loadTemplate("recetas_editar.html");
	
	include_once 'funciones.php';
	
	$datos = dameListaRecetas($userSession);
	
	echo $template->render($datos);

?>