<?php

session_start();

$userSession = $_SESSION['user'];

?>

<?php

include 'lib/config.php';

// cargamos con twig el html de login
$template = $twig->loadTemplate("login_form.html");

$datos = array(	
				'userSession' => $userSession,
			  );
			  
echo $template->render($datos);

?>