<?php

session_start(); 

$userSession = $_SESSION['user'];	

?>

<?php

include 'lib/config.php';

$template = $twig->loadTemplate("buscar.html");

$nomReceta = $_POST['nombre'];
$tipoReceta = $_POST['tipo'];
$username = $_POST['username'];

include_once 'funciones.php';

$datos = dameResultadoBusqueda($userSession, $nomReceta, $tipoReceta, $username);

echo $template->render($datos);

?>