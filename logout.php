<?php

session_start();

unset($_SESSION["user"]);

// eliminamos la session del usuario para hacer el logout
session_destroy();

// volvemos al index principal
header("Location: index.php");

exit;

?>