<?php

session_start();

$id_re = $_GET['id'];

include 'lib/config.php';

$template = $twig->loadTemplate("recetas_detalle.html");

try {

	$conn = new PDO('sqlite:recetas.sqlite3');
	
	$consulta = ' 
				SELECT *, usu.id AS id_usuario, usu.username
			     	FROM  recetas_receta
			     	JOIN  auth_user usu
			      	ON (recetas_receta.creador_id = usu.id)
						WHERE recetas_receta.id =:idre
			      ;
				';
	
	$consulta->bindParam(':idre', $idre);
	
	$idre = $id_re;
	
	$consulta->execute();
	
	$camposreceta = $consulta->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	echo $e->getMessage();
}

$conn = null;

$datos = array(
				'camposreceta' => $camposreceta,
				'userSession' => $userSession,
				);
				
echo $template->render($datos);

?>


