<?php

session_start(); 

$userSession = $_SESSION['user'];	

?>

<?php

include 'lib/config.php';

$template = $twig->loadTemplate("recetas_añadir.html");

$nomReceta = $_POST['nomReceta'];
$fechaCre = $_POST['fechaCre'];
$fechaMod = $_POST['fechaMod'];
$ingredientes = $_POST['ingredientes'];
$tipo = $_POST['tipo'];
$elaboracion = $_POST['elaboracion'];
$tiempo = $_POST['tiempo'];

include_once 'funciones.php';

$datos = añadeReceta($userSession, $nomReceta, $fechaCre, $fechaMod, $ingredientes, $tipo, $elaboracion, $tiempo);

echo $template->render($datos);

?>