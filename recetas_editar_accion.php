<?php

	session_start();
	
	$userSession = $_SESSION['user'];
	
	include 'lib/config.php';
	
	$template = $twig->loadTemplate("recetas_editar_form.html");
	
	$id_re = $_GET['id'];
	
	include_once 'funciones.php';
	
	$datos = dameRecetaPorId($userSession, $id_re);
	
	echo $template->render($datos);

?>